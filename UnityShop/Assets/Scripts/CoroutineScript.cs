﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
public class CoroutineScript : MonoBehaviour
{
    [SerializeField]
    private Image _im;
    private string url;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GetImage());
    }

    IEnumerator GetImage()
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture("https://fantasy-stockart.com/wp-content/uploads/2018/02/Preview_HumanHeavyInfantry.jpg");
        if (www.isNetworkError)
        {
            Debug.Log("Error");
            yield break;
        }
        else
        {
            Debug.Log("Success");
            yield return www;
            //Rect rect = new Rect(0, 0, www.texture.width, www.texture.height);
           //Texture texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
        }
            
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
