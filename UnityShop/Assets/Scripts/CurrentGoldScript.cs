﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentGoldScript : MonoBehaviour
{
    [SerializeField]
    private Text _currentGoldEmount;
    private void Start()
    {
       
    }
    private void Update()
    {
        _currentGoldEmount.text = "Gold: " + TransactionScript.gold;
        if (TransactionScript.gold < TransactionScript.price)
        {
            _currentGoldEmount.text = "Not enough gold";
        }
    }
}
