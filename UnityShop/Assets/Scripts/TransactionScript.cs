﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransactionScript : MonoBehaviour
{
    public static int gold=700;
    public static int price = 100;
    [SerializeField]
    private Text _unitname;
    [SerializeField]
    private Button _buy;
    [SerializeField]
    private Transform _army;
    [SerializeField]
    private Image _im;
    private List<string> _myArmy = new List<string>();
    void Start()
    {
        _buy.onClick.AddListener(UnitBought);
      
    }

    private void UnitBought()
    {
        if (price <= gold)
        {
            gold = gold - price;
            Debug.Log("Name:" + _unitname.text);
            _myArmy.Add(_unitname.text);
            // _buy.interactable = false;
            _buy.gameObject.SetActive(false);
            _unitname.transform.SetParent(_army);
            _im.transform.SetParent(_army);
        }
        else
        {
            Debug.Log("Not enough gold");
        }
         
    }
    
}
